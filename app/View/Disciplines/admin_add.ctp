<?php echo $this->Html->script('../ckeditor/ckeditor.js', array('inline' => false));?>

<h1><?php echo __("Добавить дисциплину"); ?></h1>

<?php
    $degrees = array(
        '0' => __("Бакалавр"),
        '1' => __("Магистр")
    );

    echo $this->Form->create('Discipline', array(
        'inputDefaults' => array(
            'label' => false
        )
    ));
    echo $this->Form->input('name', array(
        'label' => __('Наименование')
    ));
    echo $this->Form->input('hours', array(
        'label' => __('Количество часов')
    ));
    echo $this->Form->input('project', array(
        'label' => __('Курсовой проект')
    ));
    echo $this->Form->input('semester', array(
        'label' => __('Семестр'),
        'min' => '1',
        'value' => '1'
    ));
    echo $this->Form->input('semesters', array(
        'label' => __('Количество семестров'),
        'min' => '1',
        'value' => '1'
    ));
    echo $this->Form->input('degree',
        array(
            'options' => $degrees, 
            'default' => '0'
        )
    );
    echo $this->Form->end(__('Добавить'));
?> 
