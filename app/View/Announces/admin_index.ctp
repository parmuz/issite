<h1>Редактор объявлений</h1>

<?php echo $this->Html->link(__('Добавить'), array('action' => 'add')); ?>

<table>
    <tr>
        <th><?php echo __('ИД'); ?></th>
        <th><?php echo __('Заголовок (ru)'); ?></th>
        <th><?php echo __('Заголовок (en)'); ?></th>
        <th><?php echo __('Дата создания'); ?></th>
        <th><?php echo __('Дата модификации'); ?></th>
        <th><?php echo __('Добавил'); ?></th>
        <th><?php echo __('Действия'); ?></th>
    </tr>
    
    <?php foreach ($announces as $item): ?>
    <tr>
        <td><?php echo $item['Announce']['id']; ?></td>
        
        <td><?php echo $item['Announce']['title_ru']; ?></td>
        
        <td><?php echo $item['Announce']['title_en']; ?></td>
        
        <td><?php echo $item['Announce']['created']; ?></td>
        
        <td><?php echo $item['Announce']['modified']; ?></td>
        
        <td><?php echo $item['Announce']['user_id']; ?></td>
        
        <td>
            <?php echo $this->Html->link(__('Ред.'), 
                    array('action' => 'edit', $item['Announce']['id'])); ?>
            
            <?php echo $this->Form->postLink(__('Удалить'), 
                    array('action' => 'delete', $item['Announce']['id']),
                    array('confirm' => __('Удалить запись?'))); ?>
        </td>
    </tr>

    <?php endforeach; ?>
    <?php unset($item); ?>
</table>