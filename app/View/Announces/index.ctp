<h1>Объявления</h1>

<?php foreach ($announces as $item): ?>

<div class="shadowboxed news-block">
    <div><?php echo $item['Announce']['title_ru']; ?>
    <span style="float:right;"><?php echo $item['Announce']['created']; ?></span>
    </div>
    <hr class="separator" />
    <div><?php echo $item['Announce']['text_ru']; ?>
	<?php if(isset($item['Announce']['files'])): ?>
		<p><b>Прикрепления:</b></p>
		<?php $files = explode(" ", $item['Announce']['files']); ?>
		<?php foreach ($files as $file): ?>
		<p><?php echo $this->Html->link($file, array(
			'controller' => 'announces', 'action' => 'download', $file)
		); ?> </p>
		<?php endforeach; ?>
	<?php endif; ?>
	</div>
</div><br>

<?php endforeach; unset($item); ?>