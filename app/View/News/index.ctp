<h1>Новости</h1>

<?php foreach ($news as $item): ?>

<div class="shadowboxed news-block">
    <div><?php echo $item['News']['title_ru']; ?>
    <span style="float:right;"><?php echo $item['News']['created']; ?></span>
    </div>
    <hr class="separator" />
    <div><?php echo $item['News']['text_ru']; ?></div>
</div><br>

<?php endforeach; unset($item); ?>
