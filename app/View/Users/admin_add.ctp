<div class="users form">
<?php echo $this->Form->create('Пользователь'); ?>
	<fieldset>
		<legend><?php echo __('Добавить пользователя'); ?></legend>
	<?php
		echo $this->Form->input('username', array(
        'label' => __('Имя пользователя')
    ));
		echo $this->Form->input('password', array(
        'label' => __('Пароль')
    ));
		echo $this->Form->input('group_id', array(
        'label' => __('Группа')
    ));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Добавить')); ?>
</div>
