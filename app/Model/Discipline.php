<?php

class Discipline extends AppModel {
    public $validate = array(
        'name' => 'notEmpty',
        'hours' => 'numeric',
        'semester' => 'numeric',
        'semesters' => 'numeric'
    );
}
