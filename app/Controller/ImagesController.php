<?php

class ImagesController extends AppController {
    
    public function admin_index() {
        $this->set('image', $this->Image->find('all'));
    }
    
    public function view($id = null) {
        $this->set('image', $this->Image->findById($id));
    }
    
    public function admin_add($album_id = 1) {
        if ($this->request->is('post')) {
            $this->Image->create();
            // TODO: user_id
            $d = $this->request->data;
            $path = WWW_ROOT."img".DS;
            if (!empty($this->request->data['Image']['photo']['name'])) {
                move_uploaded_file($d['Image']['photo']['tmp_name'], 
                            $path . $d['Image']['photo']['name']);
                $d['Image']['photo'] = "img/" . $d['Image']['photo']['name']; 
            }
            $d['Image']['album_id'] = $album_id;
            if ($this->Image->save($d)) {
                $this->Session->setFlash(__("Изображение добавлено."));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__("Не удалось загрузить изображение."));
        }
    }
}