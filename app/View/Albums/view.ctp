<h1>Фотографии</h1>

<?php 
    $in_row = 6;
    $i = 0;
    
    for (; $i < count($images); $i++){
        
        if ($i != 0 && $i % $in_row == 0) {
            echo "</tr>";
            if ($i != count($images)-1) {
                echo "<tr>";
            }
        }
        
        echo '<td>'
           . '<a href="../../images/view/' . $images[$i]['Image']['id']
           . '"><img src="../../'.$images[$i]['Image']['path'].'" width=100 height=100 style="margin:10px;"></img></a>'
           . '</td>';

    } 
    
    if ($i < $in_row) {
        echo "</tr>";
    } 
?>