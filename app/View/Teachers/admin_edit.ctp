<h1><?php echo __("Редактирование информации о преподавателе"); ?></h1>

<?php
    echo $this->Form->create('Teacher', array(
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'label' => false
        )
    ));
    echo $this->Form->input('fullname', array(
        'label' => __('Полное имя')
    ));
    echo $this->Form->input('degree', array(
        'label' => __('Должность/степень')
    ));
    echo $this->Form->input('user_id', array(
        'label' => __('Пользователь')
    ));
    
    echo $this->Form->input('photo', array(
        'type' => 'file'
    ));
    
    echo $this->Form->input('id', array('type' => 'hidden'));
    echo $this->Form->end(__('Обновить'));
?> 
