<div class="users index">
	<h2><?php echo __('Пользователь'); ?></h2>
	<p class="actions">
		<?php echo $this->Html->link(__('Добавить пользователя'), array('action' => 'add')); ?>
	</p>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('ИД'); ?></th>
			<th><?php echo $this->Paginator->sort('Имя пользователя'); ?></th>
			<th><?php echo $this->Paginator->sort('Пароль'); ?></th>
			<th><?php echo $this->Paginator->sort('Группа'); ?></th>
			<th><?php echo $this->Paginator->sort('Дата создания'); ?></th>
			<th><?php echo $this->Paginator->sort('Дата редактирования'); ?></th>
			<th class="actions"><?php echo __('Действия'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['password']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
		</td>
		<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Просмотр'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Редактировать'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Удалить'), array('action' => 'delete', $user['User']['id']), array(), __('Вы уверены что хотите удалить пользователя # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Страница {:page} из {:pages}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('предыдущая'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('следующая') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
