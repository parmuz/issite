<h1>Редактор учебных планов</h1>

<?php echo $this->Html->link('Добавить', array('action' => 'add')); ?>

<table>
    <tr>
        <th><?php echo __('ИД'); ?></th>
        <th><?php echo __('Полное имя'); ?></th>
        <th><?php echo __('Должность/степень'); ?></th>
        <th><?php echo __('Пользователь'); ?></th>
        <th><?php echo __('Действия'); ?></th>
    </tr>
    
    <?php foreach ($teachers as $item): ?>
    <tr>
        <td><?php echo $item['Teacher']['id']; ?></td>
        
        <td><?php echo $item['Teacher']['fullname']; ?></td>
        
        <td><?php echo $item['Teacher']['degree']; ?></td>
        
        <td><?php echo $item['Teacher']['user_id']; ?></td>
      
        <td>
            <?php echo $this->Html->link(__('Ред.'), array('action'=>'edit',
                $item['Teacher']['id'])); ?>
            <?php echo $this->Html->link(__('Удалить'), array('action'=>'delete',
                $item['Teacher']['id'])); ?>
        </td>
    </tr>

    <?php endforeach; ?>
    <?php unset($item); ?>
</table>