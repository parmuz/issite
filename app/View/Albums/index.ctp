<h1>Альбомы</h1>

<table>
   
    <tr>
<?php 
    $in_row = 6;
    $i = 0;
    
    for (; $i < count($albums); $i++){
        
        if ($i != 0 && $i % $in_row == 0) {
            echo "</tr>";
            if ($i != count($albums)-1) {
                echo "<tr>";
            }
        }
        
        echo '<td>'
           . '<a href="albums/view/' . $albums[$i]['Album']['id']
           . '"><img src="'.$albums[$i]['Album']['thumbnail'].'" width=100 height=100></img><br><b>'
           . $albums[$i]['Album']['name'].'</b></a>'
           . '</td>';

    } 
    
    if ($i < $in_row) {
        echo "</tr>";
    } 
?>
</table>