<div class="groups index">
	<h2><?php echo __('Группы'); ?></h2>
	<p class="actions">
		<?php echo $this->Html->link(__('Добавить группу'), array('action' => 'add')); ?>
	</p>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('ИД'); ?></th>
			<th><?php echo $this->Paginator->sort('Наименование'); ?></th>
			<th><?php echo $this->Paginator->sort('Дата создания'); ?></th>
			<th><?php echo $this->Paginator->sort('Дата редактирования'); ?></th>
			<th class="actions"><?php echo __('Действия'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($groups as $group): ?>
	<tr>
		<td><?php echo h($group['Group']['id']); ?>&nbsp;</td>
		<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
		<td><?php echo h($group['Group']['created']); ?>&nbsp;</td>
		<td><?php echo h($group['Group']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Просмотр'), array('action' => 'view', $group['Group']['id'])); ?>
			<?php echo $this->Html->link(__('Редактировать'), array('action' => 'edit', $group['Group']['id'])); ?>
			<?php echo $this->Form->postLink(__('Удалить'), array('action' => 'delete', $group['Group']['id']), array(), __('Вы уверены что хотите удалить группу # %s?', $group['Group']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Страница {:page} из {:pages}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('предыдущая'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('следующая') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>