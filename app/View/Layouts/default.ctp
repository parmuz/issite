<!DOCTYPE html>
<html>
<head>
	<?php 
        echo $this->Html->charset(); 
        ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
                echo $this->Html->css('main');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
		<div id="header-main">
			Кафедра Информационных систем
		</div>
                <?php if (!strpos($this->here, 'admin')): ?>
                    <div id="menu">
                        <ul>
                            <li><?php echo $this->Html->link(__('Главная'), 
                                    array('controller'=>'pages', 'action' => 'index')) ?></li>
                            <li><?php echo $this->Html->link(__('Новости'), 
                                    array('controller'=>'news', 'action' => 'index')) ?></li>
                            <li><a href=""><?php echo $this->Html->link(__('Учебные планы'), 
                                    array('controller'=>'disciplines', 'action' => 'index')) ?></a></li>
                            <li><a href=""><?php echo $this->Html->link(__('Преподаватели'), 
                                    array('controller'=>'teachers', 'action' => 'index')) ?></a></li>
														<li><?php echo $this->Html->link(__('Объявления'),
                                    array('controller'=>'announces', 'action' => 'index')) ?></li>
                            <li><a href="">Научная работа</a></li>
                            <li><a href="">Студентам</a></li>
                            <li><a href="">Абитуриентам</a></li>
                            <li><a href=""><?php echo $this->Html->link(__('Фотоальбом'), 
                                    array('controller'=>'albums', 'action' => 'index')) ?></a></li>
                            <li><a href="">Схема проезда</a></li>
                        </ul>
                    </div>
                <?php else: ?>
                     <div id="menu">
                        <ul>
                            <li><?php echo $this->Html->link(__('Редактор новостей'), 
                                    array('controller'=>'news', 'action' => 'index')) ?></li>
                            <li><a href=""><?php echo $this->Html->link(__('Учебные планы'), 
                                    array('controller'=>'disciplines', 'action' => 'index')) ?></a></li>
                            <li><a href=""><?php echo $this->Html->link(__('Преподаватели'), 
                                    array('controller'=>'teachers', 'action' => 'index')) ?>/a></li>
                            <li><?php echo $this->Html->link(__('Редактор объявлений'),
                                    array('controller'=>'announces', 'action' => 'index')) ?></li>
                            <li><?php echo $this->Html->link(__('Фотоальбомы'), 
                                    array('controller'=>'albums', 'action' => 'index')) ?></li>
                            <li><?php echo $this->Html->link(__('Страницы'), 
                                    array('controller'=>'pages', 'action' => 'index')) ?></li>
                            <!--
                            <li><a href="">Научная работа</a></li>
                            <li><a href="">Студентам</a></li>
                            <li><a href="">Абитуриентам</a></li>
                            <li><a href="">Фотоальбом</a></li>
                            <li><a href="">Схема проезда</a></li>-->
                        </ul>
                    </div>
                <?php endif; ?>
                <div id="content">
                    
		<?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>
		</div>
            
            <center>
		<div id="footer">
			Кафедра Информационных систем (с) 2014
		</div>
            </center>
	</div>
	<?php 
        //echo $this->element('sql_dump'); 
        ?>
</body>
</html>
