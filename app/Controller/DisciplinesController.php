<?php

class DisciplinesController extends AppController {
    
    public function index($degree = 0, $semester = 1) {
        $this->set('semesters_count', $this->get_semesters_count());
        $this->set('semester', $semester);
        $this->set('degree', $degree);
        $this->set('disciplines', $this->get_disciplines($degree, $semester));
    }
    
    public function admin_index($degree = 0, $semester = 1) {
        $this->set('disciplines', $this->Discipline->find('all', array(
            'order' => 'Discipline.semester ASC'
        )));
    }
    
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Discipline->create();
            // TODO: user_id
            if ($this->Discipline->save($this->request->data)) {
                $this->Session->setFlash(__("Дисциплина добавлена"));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__("Не удалось добавить дисциплину."));
        }
    }
    
    public function admin_edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Дисциплина не найдена.'));
        }
        
        $post = $this->Discipline->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Дисциплина не найдена.'));
        }
        
        if ($this->request->is(array('post', 'put'))) {
            $this->Discipline->id = $id;
            if ($this->Discipline->save($this->request->data)) {
                $this->Session->setFlash(__('Информация о дисциплине обновлена.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Ошибка при обновлении информации.'));
        }
        
        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }
    
    public function admin_delete($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Дисциплина не найдена.'));
        }
        
        if ($this->Discipline->delete($id)) {
            $this->Session->setFlash(__('Дисциплина с id: %s была удалена.', h($id)));
            return $this->redirect(array('action' => 'index'));
        }
        
    }
    
    private function get_disciplines($degree, $semester) {
        $disps = $this->Discipline->find('all', array(
            'conditions' => array(
                'Discipline.degree' => $degree,
                'Discipline.semester <=' => $semester,
                '(Discipline.semester + Discipline.semesters) >' => $semester
            )
        ));
        
        return $disps;
    }
    
    private function get_semesters_count() {
        $semesters = $this->Discipline->find('all', array(
            'order' => 'Discipline.semester DESC',
            'fields' => array(
                'DISTINCT Discipline.semester',
                'Discipline.semesters'
            )
        ));
       
        $result = (int)$semesters[0]['Discipline']['semester' ] + 
                  (int)$semesters[0]['Discipline']['semesters'] - 1;
        
        return $result;
    }
}