<?php

class UsersController extends AppController {
	public $components = array('Paginator');

	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Неверное имя пользователя'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Пользователь успешно создан'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Пользователь не может быть создан. Попробуйте ещё раз.'));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Неверное имя пользователя'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Пользователь успешно отредактирован'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Пользователь не может быть отредактирован. Попробуйте ещё раз.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Неверное имя пользователя'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('Пользователь успешно удален'));
		} else {
			$this->Session->setFlash(__('Пользователь не может быть удален. Попробуйте ещё раз.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function login() {
		if ($this->Session->read('Auth.User')) {
			$this->Session->setFlash('Вы успешно авторизовались');
			return $this->redirect('/');
		}
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirect());
			}
			$this->Session->setFlash(__('Неверное имя пользователя или пароль'));
		}
	}

	public function logout() {
		$this->Session->setFlash('Вы успешно вышли');
		$this->redirect($this->Auth->logout());
	}

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow();
	}
}