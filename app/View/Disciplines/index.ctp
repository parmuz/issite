<h1>Учебный план</h1>

<?php 
for ($i = 1; $i <= $semesters_count+1; $i++){ 
    if ($i == $semester) {
        echo $i . " ";
        continue;
    }
    
    echo $this->Html->link($i, array(
                'controller' => 'disciplines', 
                'action' => 'index', $degree, $i
            )) . " ";
}
?>

<table>
    <tr>
        <th><?php echo __('Наименование'); ?></th>
        <th><?php echo __('Часов'); ?></th>
        <th><?php echo __('Курсовой проект'); ?></th>
        <th><?php echo __('Семестр'); ?></th>
        <th><?php echo __('Кол-во семестров'); ?></th>
    </tr>
    
    <?php foreach ($disciplines as $item): ?>
    <tr>
        <td><?php echo $item['Discipline']['name']; ?></td>
        
        <td><?php echo $item['Discipline']['hours']; ?></td>
        
        <td><?php echo $item['Discipline']['project'] ? "Да" : "Нет"; ?></td>
        
        <td><?php echo $item['Discipline']['semester']; ?></td>
        
        <td><?php echo $item['Discipline']['semesters']; ?></td>
      
    </tr>

    <?php endforeach; ?>
    <?php unset($item); ?>
</table>