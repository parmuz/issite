<div class="groups form">
<?php echo $this->Form->create('Group'); ?>
	<fieldset>
		<legend><?php echo __('Редактирование группы'); ?></legend>
	<?php
		echo $this->Form->input('id', array(
        'label' => __('ИД')
    ));
		echo $this->Form->input('name', array(
        'label' => __('Наименование')
    ));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Обновить')); ?>
</div>
