<?php

class Question extends AppModel {
	public $validate = array(
		'text' => array(
			'rule' => 'notEmpty'
		)
	);
}