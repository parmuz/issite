<?php

class TeachersController extends AppController {
    
    public function index() {
        $this->set('teachers', $this->Teacher->find('all'));
    }
    
    public function admin_index() {
        $this->set('teachers', $this->Teacher->find('all'));
    }
    
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Teacher->create();
            // TODO: user_id
            
            $d = $this->request->data;
            $path = WWW_ROOT."img".DS;
            if (!empty($this->request->data['Teacher']['photo']['name'])) {
                move_uploaded_file($d['Teacher']['photo']['tmp_name'], 
                            $path . $d['Teacher']['photo']['name']);
                $d['Teacher']['photo'] = "img/" . $d['Teacher']['photo']['name'];
            }
                
            if ($this->Teacher->save($d)) {
                $this->Session->setFlash(__("Информация о преподавателе добавлена."));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__("Не удалось добавить информацию о преподавателе."));
        }
    }
    
    public function admin_edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Информация о преподавателе не найдена.'));
        }
        
        $entry = $this->Teacher->findById($id);
        if (!$entry) {
            throw new NotFoundException(__('Информация о преподавателе не найдена.'));
        }
        
        if ($this->request->is(array('post', 'put'))) {
            $d = $this->request->data;
            $path = WWW_ROOT."img".DS;
            if (!empty($this->request->data['Teacher']['photo']['name'])) {
                unlink($entry['Teacher']['photo']);
                
                move_uploaded_file($d['Teacher']['photo']['tmp_name'], 
                            $path . $d['Teacher']['photo']['name']);
                $d['Teacher']['photo'] = "img/" . $d['Teacher']['photo']['name'];
            }
            
            $this->Teacher->id = $id;
            if ($this->Teacher->save($d)) {
                $this->Session->setFlash(__('Информация о преподавателе обновлена.'));
                return $this->redirect(array('action' => 'index'));
            }
            
            $this->Session->setFlash(__('Ошибка при обновлении информации.'));
        }
        
        if (!$this->request->data) {
            $this->request->data = $entry;
        }
    }
    
    public function admin_delete($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Информация о преподавателе не найдена.'));
        }
        
        $entry = $this->Teacher->findById($id);
        if (!$entry) {
            throw new NotFoundException(__('Информация о преподавателе не найдена.'));
        }
        
        unlink($entry['Teacher']['photo']);
        
        if ($this->Teacher->delete($id)) {
            $this->Session->setFlash(__('Информация о преподавателе с id: %s была удалена.', h($id)));
            return $this->redirect(array('action' => 'index'));
        }
        
    }
    
    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Информация о преподавателе не найдена.'));
        }
        
        $entry = $this->Teacher->findById($id);
        if (!$entry) {
            throw new NotFoundException(__('Информация о преподавателе не найдена.'));
        }
        
        $this->set('teacher', $entry);
    }
    
}
