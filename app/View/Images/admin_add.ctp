<h1>Добавить изображение</h1>

<?php
    echo $this->Form->create('Image', array(
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'label' => false
        )
    ));
    echo $this->Form->input('name', array(
        'label' => __('Название')
    ));
    /*
    echo $this->Form->input('album_id', array(
        'type' => 'hidden',
        'value' => $this->request->pass[0]
    ));*/
    
    echo $this->Form->input('photo', array(
        'type' => 'file'
    ));
    
    echo $this->Form->end(__('Добавить'));
?> 
