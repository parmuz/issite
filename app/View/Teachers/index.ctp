<h1>Преподаватели</h1>

<table>
   
    <tr>
<?php 
    $in_row = 4;
    $i = 0;
    
    for (; $i < count($teachers); $i++){
        
        if ($i != 0 && $i % $in_row == 0) {
            echo "</tr>";
            if ($i != count($teachers)-1) {
                echo "<tr>";
            }
        }
        
        echo '<td>'
           . '<a href="teachers/view/' . $teachers[$i]['Teacher']['id']
           . '"><img src="'.$teachers[$i]['Teacher']['photo'].'"></img></a>'
           . '</td>';

    } 
    
    if ($i < $in_row) {
        echo "</tr>";
    } 
?>
</table>