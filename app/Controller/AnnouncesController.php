<?php

class AnnouncesController extends AppController {
    public $helpers = array('Html', 'form');
		
    public function index() {
        $this->set('announces', $this->Announce->find('all', array(
            'order' => 'Announce.created DESC'
        )));
    }

    public function admin_index() {
        $this->set('announces', $this->Announce->find('all', array(
            'order' => 'Announce.created DESC'
        )));
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Announce->create();
			$d = $this->request->data;
			$filepath = "";
			foreach ($d['Announce']['files'] as $file) {
			$path = WWW_ROOT."files".DS;
				if ($file['error'] == UPLOAD_ERR_OK) {
					$tmp_name = $file['tmp_name'];
					$name = $file['name'];
					move_uploaded_file($tmp_name, $path . "$name");
					$filepath .= $name . " ";
				}
			}
			$d['Announce']['files'] = trim($filepath);
            if ($this->Announce->save($d)) {
                $this->Session->setFlash(__("Объявление добавлено"));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__("Не удалось разместить объявление."));
        } 
    }

	public function download($file) {
		$this->response->file("files" . DS . $file, array('download' => true, 'name' => $file));
		return $this->response;
	}

    public function admin_edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Такое объявление не обнаружена.'));
        }

        $item = $this->Announce->findById($id);
        if (!$item) {
            throw new NotFoundException(__('Такое объявление не обнаружено.'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Announce->id = $id;
            if ($this->Announce->save($this->request->data)) {
                $this->Session->setFlash(__('Объявление отредактировано'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Не удалось отредактировать объявление.'));
        }

        if (!$this->request->data) {
            $this->request->data = $item;
        }
    }

    public function admin_delete($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Такое объявление не обнаружено.'));
        }

        if ($this->Announce->delete($id)) {
            $this->Session->setFlash(__('Объявление с id: %s было удалено.', h($id)));
            return $this->redirect(array('action' => 'index'));
        }
    }
}