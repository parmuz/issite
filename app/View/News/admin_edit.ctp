<?php echo $this->Html->script('../ckeditor/ckeditor.js', array('inline' => false));?>

<h1><?php echo __("Редактирование новости"); ?></h1>

<?php
    echo $this->Form->create('News', array(
        'inputDefaults' => array(
            'label' => false,
            'div' => false
        )
    ));
    echo $this->Form->input('title_ru', array(
        'placeholder' => __('Заголовок')
    ));
    echo $this->Form->input('text_ru', array(
        'rows' => '4',
        'placeholder' => __('Текст'),
        'id' => 'data[News][text_ru]'
    ));
    echo $this->Form->input('id', array('type' => 'hidden'));
    echo $this->Form->end(__('Обновить'));
?> 

<script> CKEDITOR.replace('data[News][text_ru]'); </script>
