<?php

class News extends AppModel {
    public $validate = array(
        'title_ru' => array(
            'rule' => 'notEmpty'
        ),
        'text_ru' => array(
            'rule' => 'notEmpty'
        )
    );
}