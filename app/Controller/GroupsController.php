<?php

class GroupsController extends AppController {
	public $components = array('Paginator');

	public function index() {
		$this->Group->recursive = 0;
		$this->set('groups', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Неверная группа'));
		}
		$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
		$this->set('group', $this->Group->find('first', $options));
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->Group->create();
			if ($this->Group->save($this->request->data)) {
				$this->Session->setFlash(__('Группа успешно создана'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Группа не может быть создана. Попробуйте ещё раз.'));
			}
		}
	}

	public function edit($id = null) {
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Неверная группа'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Group->save($this->request->data)) {
				$this->Session->setFlash(__('Группа успешно отредактирована'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Группа не может быть отредактирована. Попробуйте ещё раз.'));
			}
		} else {
			$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
			$this->request->data = $this->Group->find('first', $options);
		}
	}

	public function delete($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Неверная групп'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Group->delete()) {
			$this->Session->setFlash(__('Группа успешно удалена.'));
		} else {
			$this->Session->setFlash(__('Группа не может быть удалена. Попробуйте ещё раз.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}