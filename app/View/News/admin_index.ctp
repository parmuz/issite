<h1>Редактор новостей</h1>

<?php echo $this->Html->link(__('Добавить'), array('action' => 'add')); ?>

<table>
    <tr>
        <th><?php echo __('ИД'); ?></th>
        <th><?php echo __('Заголовок (ru)'); ?></th>
        <th><?php echo __('Заголовок (en)'); ?></th>
        <th><?php echo __('Дата создания'); ?></th>
        <th><?php echo __('Дата модификации'); ?></th>
        <th><?php echo __('Добавил'); ?></th>
        <th><?php echo __('Действия'); ?></th>
    </tr>
    
    <?php foreach ($news as $item): ?>
    <tr>
        <td><?php echo $item['News']['id']; ?></td>
        
        <td><?php echo $item['News']['title_ru']; ?></td>
        
        <td><?php echo $item['News']['title_en']; ?></td>
        
        <td><?php echo $item['News']['created']; ?></td>
        
        <td><?php echo $item['News']['modified']; ?></td>
        
        <td><?php echo $item['News']['user_id']; ?></td>
        
        <td>
            <?php echo $this->Html->link(__('Ред.'), 
                    array('action' => 'edit', $item['News']['id'])); ?>
            
            <?php echo $this->Form->postLink(__('Удалить'), 
                    array('action' => 'delete', $item['News']['id']),
                    array('confirm' => __('Удалить запись?'))); ?>
        </td>
    </tr>

    <?php endforeach; ?>
    <?php unset($item); ?>
</table>