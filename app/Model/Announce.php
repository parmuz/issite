<?php

class Announce extends AppModel {
	public $validate = array(
		'title_ru' => array(
			'rule' => 'notEmpty'
		),
		'text_ru' => array(
			'rule' => 'notEmpty'
		),
		'files.' => array (
			'rule' => array(
				'allowEmpty' => TRUE
			)
		)
	);
}