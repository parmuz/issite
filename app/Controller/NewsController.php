<?php

class NewsController extends AppController {
    public $helpers = array('Html', 'form');
    
    public function index() {
        $this->set('news', $this->News->find('all', array(
            'order' => 'News.created DESC'
        )));
    }
    
    public function admin_index() {
        $this->set('news', $this->News->find('all', array(
            'order' => 'News.created DESC'
        )));
    }
    
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->News->create();
            // TODO: user_id
            if ($this->News->save($this->request->data)) {
                $this->Session->setFlash(__("Новость добавлена"));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__("Не удалось разместить новость."));
        }
    }
    
    public function admin_edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Такая новость не обнаружена.'));
        }
        
        $item = $this->News->findById($id);
        if (!$item) {
            throw new NotFoundException(__('Такая новость не обнаружена.'));
        }
        
        if ($this->request->is(array('post', 'put'))) {
            $this->News->id = $id;
            if ($this->News->save($this->request->data)) {
                $this->Session->setFlash(__('Новость обновлена'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Не удалось обновить новость.'));
        }
        
        if (!$this->request->data) {
            $this->request->data = $item;
        }
    }
    
    public function admin_delete($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Такая новость не обнаружена.'));
        }
        
        if ($this->News->delete($id)) {
            $this->Session->setFlash(__('Новость с id: %s была удалена.', h($id)));
            return $this->redirect(array('action' => 'index'));
        }
    }
}
