<h1>Редактор учебных планов</h1>

<?php echo $this->Html->link('Добавить', array('action' => 'add')); ?>

<table>
    <tr>
        <th><?php echo __('ИД'); ?></th>
        <th><?php echo __('Наименование'); ?></th>
        <th><?php echo __('Часов'); ?></th>
        <th><?php echo __('Курсовой проект'); ?></th>
        <th><?php echo __('Семестр'); ?></th>
        <th><?php echo __('Кол-во семестров'); ?></th>
        <th><?php echo __('Действия'); ?></th>
    </tr>
    
    <?php foreach ($disciplines as $item): ?>
    <tr>
        <td><?php echo $item['Discipline']['id']; ?></td>
        
        <td><?php echo $item['Discipline']['name']; ?></td>
        
        <td><?php echo $item['Discipline']['hours']; ?></td>
        
        <td><?php echo $item['Discipline']['project']; ?></td>
        
        <td><?php echo (int)$item['Discipline']['semester']; ?></td>
        
        <td><?php echo $item['Discipline']['semesters']; ?></td>
      
        <td>
            <?php echo $this->Html->link(__('Ред.'), array('action'=>'edit',
                $item['Discipline']['id'])); ?>
            <?php echo $this->Html->link(__('Удалить'), array('action'=>'delete',$item['Discipline']['id']
                    )); ?>
        </td>
    </tr>

    <?php endforeach; ?>
    <?php unset($item); ?>
</table>