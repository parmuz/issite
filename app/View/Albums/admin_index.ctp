<h1>Редактирование альбомов</h1>

<?php echo $this->Html->link('Добавить', array('action' => 'add')); ?>

<table>
    <tr>
        <th><?php echo __('ИД'); ?></th>
        <th><?php echo __('Имя'); ?></th>
        <th><?php echo __('Путь'); ?></th>
        <th><?php echo __('Альбом'); ?></th>
        <th><?php echo __('Действия'); ?></th>
    </tr>
    
    <?php foreach ($albums as $item): ?>
    <tr>
        <td><?php echo $item['Album']['id']; ?></td>
        
        <td><?php echo $item['Album']['name']; ?></td>
        
        <td><?php echo $item['Album']['description']; ?></td>
        
        <td><?php echo $item['Album']['created']; ?></td>
      
        <td>
            <?php echo $this->Html->link(__('Добавить'), array(
                'controller' => 'images', 
                'action'=>'add',
                $item['Album']['id'])); ?>
            <?php echo $this->Html->link(__('Изменить'), array('action'=>'edit',
                $item['Album']['id'])); ?>
            <?php echo $this->Html->link(__('Удалить'), array('action'=>'delete',
                $item['Album']['id'])); ?>
        </td>
    </tr>

    <?php endforeach; ?>
    <?php unset($item); ?>
</table>