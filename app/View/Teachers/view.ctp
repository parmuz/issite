<h1>Информация о преподавателе</h1>

<img src="<?php echo "../../".$teacher['Teacher']['photo']; ?>" style="float:left; margin:5xp;"></img>

<span style="margin: 5px; font-size: 14pt;"><?php echo $teacher['Teacher']['fullname']; ?></span><br>
<span style="margin: 5px;"><?php echo $teacher['Teacher']['degree']; ?></span>
<?php
    echo $this->Form->create('Question', array(
		'url' => array('controller' => 'questions', 'action' => 'add'),
		'inputDefaults' => array(
 			'label' => false,
			'div' => false
        )
	));

    echo $this->Form->input('text', array(
        'type' => 'text',
		'label' => 'Задать вопрос',
		'placeholder' => __('Вопрос')
    ));

	echo $this->Form->input('email', array(
		'type' => 'email',
		'label' => 'Введите свой email'
	));
	echo $this->Form->input('user_id', array(
		'type' => 'hidden',
		'value' => $teacher['Teacher']['user_id']
	));

	echo $this->Form->end(__('Задать'));
?>