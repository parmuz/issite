<?php

class QuestionsController extends AppController {
	public function add() {
		$this->autoRender = false;
        if ($this->request->is('post')) {
            $this->Question->create();
            // TODO: user_id
            $d = $this->request->data;
			if ($this->Question->save($d)) {
                $this->Session->setFlash(__("Вопрос отправлен"));
            }
            else
				$this->Session->setFlash(__("Не удалось отправить вопрос."));
        }
		$this->redirect($this->referer());
	}
}