<?php

class AlbumsController extends AppController {
    
    public function index() {
        $this->set('albums', $this->Album->find('all'));
    }
    
    public function admin_index() {
        $this->set('albums', $this->Album->find('all'));
    }
    
    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Альбом не найден.'));
        }
        
        $entry = $this->Album->findById($id);
        if (!$entry) {
            throw new NotFoundException(__('Альбом не найден.'));
        }
        
        $images = ClassRegistry::init('Image')->find('all', array(
            'condition' => array(
                'Image.album_id' => $id
            )
        ));
        
        $this->set('images', $images);
    }

}
